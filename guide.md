
#Restaurant CMS system
This project is based on TypeScript Node Starter
[link](https://github.com/Microsoft/TypeScript-Node-Starter)

#How to run
- Install dependencies: ```npm install```
- Install typescript: ```npm install -D typescript```
- Run mongod
- Run ```npm start``` then open browser ```localhost:3000```
- Testing by ```npm test```

#Features
- Restaurant list page with table view. 
- RESTFUL API to update/delete/get detail of restaurant. Only get endpoint is public and able test via CURL. Other endpoint required login(using authenticated middleware).
- Create new restaurant with basic information and store in MongoDB. Simple validation was introduced.

#Technologies
- Typescript to write and compile to javascript in dist folder
- Pug is template engine.
- Router is using expressjs
- Other NodeJS libraries like mongoose, passport...
- Database is using mongodb. 

#New Files
- views/restaurant/detail.pug
- views/restaurant/list.pug
- controllers/restaurant.ts
- models/Restaurant.ts
- server.ts

