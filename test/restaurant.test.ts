import {} from 'jest';
import * as supertest from "supertest";
const request = supertest("http://localhost:8000");

describe("GET /restaurant/list", () => {
    it("should return 200 OK", (done) => {
      request.get("/restaurant/list")
        .expect(200, done);
    });
  });